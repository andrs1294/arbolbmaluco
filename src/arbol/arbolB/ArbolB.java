/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ $Id:
 * ArbolB.java,v 1.2 2008/09/22 20:29:21 alf-mora Exp $ Universidad de los Andes
 * (Bogot� - Colombia) Departamento de Ingenier�a de Sistemas y Computaci�n
 * Licenciado bajo el esquema Academic Free License version 2.1
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co) Framework: Cupi2Collections
 * Autor: Jorge Villalobos - 25/02/2006 Autor: Pablo Barvo - 25/02/2006
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package arbol.arbolB;

import java.io.Serializable;

import iterador.Iterador;
import iterador.IteradorException;
import iterador.IteradorSimple;
import arbol.ElementoExisteException;
import arbol.ElementoNoExisteException;
import arbol.IArbolOrdenado;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementaci�n de un �rbol B
 *
 * @param <T> Tipo de datos que contiene cada nodo del �rbol. Los nodos deben
 * implementar la interface Comparable
 */
public class ArbolB<T extends Comparable<? super T>> implements Serializable, IArbolOrdenado<T> {
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Constante para la serializaci�n
     */
    private static final long serialVersionUID = 1L;

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
    /**
     * Ra�z del �rbol B
     */
    private NodoB<T> raiz;

    /**
     * Peso del �rbol B
     */
    private int peso;

    /**
     * Orden del �rbol B
     */
    private int orden;

    static String caminoBusqueda = "";

    private Vector<Integer> s1;
    private Vector<Integer> s2;
    private Vector<Integer> menores;
    public int pathSearch[];
    private Vector<Integer> mayores;
    public ArbolB<T> T1;
    public ArbolB<T> T2;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Construye un nuevo �rbol vac�o. <br>
     * <b>post: </b> Se construy� un �rbol vac�o, con ra�z null.
     */
    public ArbolB(int orden) {
        raiz = null;
        this.orden = orden;
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------
    /**
     * Devuelve la ra�z del �rbol para ser navegada. <br>
     * <b>post: </b> Se retorn� la ra�z del �rbol.
     *
     * @return Ra�z del �rbol
     */
    public NodoB<T> darRaiz() {
        return raiz;
    }

          /**
     * Name: buscar
     * Purpose: buscar dentro del arbol un T dado
     * Inputs: T, valor a buscar
     * Outputs: T encontrado o no
     * Complexity: O(n), en donde n es el tamaño del P resultante
     *  
    */   
    
    public T buscar(T modelo) {
        T resp;
        resp = (raiz != null) ? raiz.buscar(modelo) : null;

        ArbolB.caminoBusqueda = ArbolB.caminoBusqueda.substring(1); //Quito el primer _ que se coloca
        ArbolB.caminoBusqueda = ArbolB.caminoBusqueda.replaceAll("null", "");
        ArbolB.caminoBusqueda = ArbolB.caminoBusqueda.replaceAll("__", "_");
        String num[] = ArbolB.caminoBusqueda.split("_");
        pathSearch = new int[num.length];
        int c = 0;
        for (String num1 : num) {
            if (!num1.equals("null")) {
                pathSearch[c++] = Integer.parseInt(num1);
            }
        }
        return resp;
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbol#darAltura()
     */
    public int darAltura() {
        return raiz == null ? 0 : raiz.darAltura();
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbol#darPeso()
     */
    public int darPeso() {
        return peso;
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbolOrdenado#insertar(java.lang.Comparable)
     */
    public void insertar(T elemento) throws ElementoExisteException {
        if (raiz == null) {
            // Caso 1: el �rbol es vac�o
            raiz = new NodoB<T>(elemento, orden);
        } else {
            // Caso 2: el �rbol no es vac�o
            raiz = raiz.insertar(elemento);
        }
        peso++;
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbolOrdenado#eliminar(java.lang.Comparable)
     */
    public void eliminar(T elemento) throws ElementoNoExisteException {
        if (raiz != null) {
            // Caso 1: el �rbol no es vac�o

            NodoB aux = raiz.eliminar(elemento);
//            if(raiz!=aux)
//            {
//                aux.setAltura(aux.getAltura()-1);
//            }
            raiz = aux;
            peso--;
        } else {
            // Caso 2: el �rbol es vac�o
            throw new ElementoNoExisteException("El elemento especificado no existe en el �rbol");
        }
    }

              /**
     * Name: crearConjuntoArboles
     * Purpose: crea los conjuntos de arboles necesario para la operacion split
     * Inputs: p, camino de busqueda
     *         s, conjunto de claves del arbol 
     * Outputs: conjuntos s' y s''
     * Complexity: O(n), en donde n es el tamaño de s
     *  
    */   
    
    private Vector<ArbolB<T>> crearConjuntoArboles(int p[], int[] s) {
        int c = 0;
        Vector<ArbolB<T>> ret = new Vector<>();
        if (p.length == 0) {
            ArbolB<T> arbol = new ArbolB(4);
            for (int ret1 : s) {
                try {
                    arbol.insertar((T) ((Integer) (ret1)));
                } catch (ElementoExisteException ex) {
                    Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ret.add(arbol);
            return ret;
        }

        for (int i = 0; i < s.length; i++) {
            ArbolB<T> arbol = new ArbolB(4);
            while (i < s.length && s[i] != p[c]) {
                try {
                    arbol.insertar((T) ((Integer) (s[i])));
                    i++;
                } catch (ElementoExisteException ex) {
                    Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            c++;
            ret.add(arbol);
            if (c == p.length) {
                ArbolB<T> ultimo = new ArbolB<>(4);
                for (; i < s.length; i++) {
                    try {
                        if (s[i] != p[c-1]) {
                            ultimo.insertar((T) ((Integer) (s[i])));
                        }
                    } catch (ElementoExisteException ex) {
                        Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                ret.add(ultimo);
            }

        }

        return ret;
    }

    /**
     * Devuelve los elementos del �rbol en inorden. <br>
     * <b>post: </b> Se retorno el iterador para recorrer los elementos del
     * �rbol en inorden.
     *
     * @return Iterador para recorrer los elementos del �rbol en inorden.
     * Diferente de null.
     */
    public Iterador<T> inorden() {
        IteradorSimple<T> resultado = new IteradorSimple<T>(peso);
        if (raiz != null) {
            try {
                raiz.inorden(resultado);
            } catch (IteradorException e) {
                // Nunca deber�a lanzar esta excepci�n porque el tama�o del
                // iterador es el peso del �rbola
            }
        }
        return resultado;
    }
    
    /**
     * Devuelve los elementos del �rbol en inorden. <br>
     * <b>post: </b> Se retorno el iterador para recorrer los elementos del
     * �rbol en inorden.
     *
     * @return Iterador para recorrer los elementos del �rbol en inorden.
     * Diferente de null.
     */
    public LinkedList<T> inorden2() {
        LinkedList<T> resultado = new LinkedList<>();
        if (raiz != null) {
       
                raiz.inorden2(resultado);
            
        }
        return resultado;
    }
    
                  /**
     * Name: join
     * Purpose: ejecuta la operacion join entre dos arboles y una llave dada k
     * Inputs: x, valor del medio 
     *         T1 arbol 1 
     *         T2 arbol 2 
     * Outputs: Arbol resultante de la unificacion 
     * Complexity: O(1 +   |ℎ1 − ℎ2|) en donde h1 es la altura de t1 y h2 la altura de t2
     *  
    */   

    public ArbolB<T> join(int x, ArbolB T1, ArbolB T2) throws ElementoExisteException {
        int resta = -1;
        if (T1.raiz != null && T2.raiz != null) {
            resta = T1.raiz.getAltura() - T2.raiz.getAltura();
        } else {
            if (T1.raiz == null && T2.raiz != null) {
                T2.insertar(x);
                return T2;
            }else if(T1.raiz!=null && T2.raiz==null)
            {
                T1.insertar(x);
                return T1;
            }
        }

        if (resta > 0) {
            //NodoB z = T1.raiz.getNodoMasDerechaAlNivel(1,resta);

            T1.setRaiz(T1.raiz.insertarMaluco(x, resta, resta, T2.raiz));
            return T1;
        } else if (resta == 0) {
            T1.setRaiz(T1.raiz.insertarMaluco(x, 1, resta, T2.raiz));
            return T1;
        } else if (resta < 0) {

            T2.setRaiz(T2.raiz.insertarMaluco(x, (resta * -1), resta, T1.raiz));
            return T2;

        }

        return null;

    }

    /**
     * Llena los conjuntos S1 y S2
     *
     * @param k valar de particion del arbol
     */
    private void obtenerS1yS2(int k) {
        LinkedList<T> i =  inorden2();

        for (T i1 : i) {
            int numero = (Integer)i1;
            if (numero < k) {
                s1.add(numero);
            } else if (numero > k) {
                s2.add(numero);
            } else {
                continue;
            }
        }
        
        

    }

                      /**
     * Name: split 
     * Purpose: ejecuta la operacion split entre un arbol dado y una k
     * Inputs: x, valor que parte el arbol
     * Outputs: Arbol resultante de la particion 
     * Complexity: O(log(n)) en donde n es la cantidad de nodos 
     *  
    */ 
    
    public void split(Integer k) {
        this.s1 = new Vector<>();
        this.s2 = new Vector<>();
        this.menores = new Vector<>();
        this.mayores = new Vector<>();

        buscar((T) k);
        obtenerS1yS2(k);
        partirP(pathSearch, k);
        int pMen[] = convertirAarreglo(menores);
        int pMay[] = convertirAarreglo(mayores);
        int s1arry[] = convertirAarreglo(s1);
        int s2arry[] = convertirAarreglo(s2);
        Vector<ArbolB<T>> Arboles1 = crearConjuntoArboles(pMen, s1arry);
        Vector<ArbolB<T>> Arboles2 = crearConjuntoArboles(pMay, s2arry);

        
         T1 = new ArbolB<>(4);
         T2 = new ArbolB<>(4);
        
        crearArbolSplit(pMen, T1, Arboles1);
        crearArbolSplit(pMay, T2, Arboles2);
        caminoBusqueda="";
        
    }

                          /**
     * Name: crearArbolSplit 
     * Purpose: dado un conjunto de arboles y el camino P retorna el join de todos los arboles
     * Inputs: pMen, camino de busqueda
     *         T1, arbol 1
     *         Arboles 1, conjunto de arboles 
     * Outputs: oin de todos los arboles
     * Complexity: O(n) en donde n es el tamaño de pMen 
     *  
    */ 
    private void crearArbolSplit(int[] pMen, ArbolB<T> T1, Vector<ArbolB<T>> Arboles1) {
        if(pMen.length==0)
        {
            T1.setRaiz(Arboles1.get(0).raiz);
        }else
        {
            try {
                T1.setRaiz(T1.join(pMen[0], Arboles1.get(0), Arboles1.get(1)).raiz);
            } catch (ElementoExisteException ex) {
                Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (int i = 1; i < pMen.length; i++) {
                try {
                    T1.setRaiz(T1.join(pMen[i], T1, Arboles1.get(i+1)).raiz);
                } catch (ElementoExisteException ex) {
                    Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * 
     *
     * @param p es el camino por el cual se busco la clave k
     * @param k es la clave que se busco
     *                           /**
     * Name: partirP 
     * Purpose: particiona P en base a K y su respectiva relacion con k
     * Complexity: O(n) en donde n es el tamaño de p 
     *  
    */ 
     
    private void partirP(int p[], int k) {

        for (int i = 0; i < p.length; i++) {
            if (p[i] < k) {
                menores.add(p[i]);
            } else if (p[i] > k) {
                mayores.add(p[i]);
            } else {
                continue;
            }
        }
        Collections.sort(menores);
        Collections.sort(mayores);

    }

    public void setRaiz(NodoB<T> raiz) {
        this.raiz = raiz;
    }

                              /**
     * Name: convertirAarreglo 
     * Purpose: dado un vector<T> se conbierte a arrglo normal
     * Inputs: menores, es el vector a convertir 
     * Outputs: vector convencional
     * Complexity: O(n) en donde n es el tamaño del vector entrante
     *  
    */ 
    
    private int[] convertirAarreglo(Vector<Integer> menores) {
        int arre[] = new int[menores.size()];

        for (int i = 0; i < menores.size(); i++) {
            arre[i] = menores.get(i);
        }
        return arre;
    }

}
