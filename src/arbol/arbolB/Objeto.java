/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol.arbolB;

import java.io.File;

/**
 *
 * @author andrs1294
 */
public class Objeto implements Comparable<Object> {

    private Object hol;

    public Objeto(Object hol) {
        this.hol = hol;
    }

    public Object getObjeto() {
        return hol;
    }

    public void setObjeto(Object hol) {
        this.hol = hol;
    }

    @Override
    public String toString() {
        if (hol instanceof Integer) {
            int a = (Integer) hol;
            return "" + a;
        } else if (hol instanceof File) {
            File a = (File) hol;
            return "Archivo: " + a.getName();
        } else if (hol instanceof String) {
            
            return hol.toString();
        }else
        {
            return "otro";
        }
    }

    @Override
    public int hashCode() {
       if(this.getObjeto() instanceof Integer)
       {
           return (int)(this.getObjeto());
       }
       return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getObjeto() instanceof Integer) {
            int hash = ((Objeto) (obj)).hashCode();
            return hash == (int) (this.getObjeto());
        }
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Object o) {
        Objeto os = (Objeto) (o);    
        return this.hashCode()-os.hashCode();
    }

}
